import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Page2buttonsComponent } from './page2buttons.component';

describe('Page2buttonsComponent', () => {
  let component: Page2buttonsComponent;
  let fixture: ComponentFixture<Page2buttonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Page2buttonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Page2buttonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
