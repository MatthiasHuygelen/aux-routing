import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidebarComponent } from './sidebar/sidebar.component';
import { Sidebar2Component } from './sidebar2/sidebar2.component';
import { PageComponent } from './page/page.component';
import { Page2Component } from './page2/page2.component';
import { OptionAComponent } from './option-a/option-a.component';
import { OptionBComponent } from './option-b/option-b.component';
import { OptionCComponent } from './option-c/option-c.component';
import { Page2buttonsComponent } from './page2buttons/page2buttons.component';

const routes: Routes = [
  { path: '', component: SidebarComponent, outlet: 'test' },
  { path: '2', component: Sidebar2Component, outlet: 'test' },
  {
    path: '', component: PageComponent
  },
  {
    path: '2', component: Page2Component, children: [
      {
        path: '', component: Page2buttonsComponent, pathMatch: 'full'
      },
      {
        path: 'A', component: OptionAComponent
      },
      {
        path: 'B', component: OptionBComponent
      },
      {
        path: 'C', component: OptionCComponent
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
