import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { Sidebar2Component } from './sidebar2/sidebar2.component';
import { PageComponent } from './page/page.component';
import { Page2Component } from './page2/page2.component';
import { OptionAComponent } from './option-a/option-a.component';
import { OptionBComponent } from './option-b/option-b.component';
import { OptionCComponent } from './option-c/option-c.component';
import { Page2buttonsComponent } from './page2buttons/page2buttons.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    Sidebar2Component,
    PageComponent,
    Page2Component,
    OptionAComponent,
    OptionBComponent,
    OptionCComponent,
    Page2buttonsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
